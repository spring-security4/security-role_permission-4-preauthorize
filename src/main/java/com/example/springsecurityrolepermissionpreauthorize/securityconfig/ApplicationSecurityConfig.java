package com.example.springsecurityrolepermissionpreauthorize.securityconfig;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import static com.example.springsecurityrolepermissionpreauthorize.securityconfig.UserPermission.*;
import static com.example.springsecurityrolepermissionpreauthorize.securityconfig.UserRole.*;
import static com.example.springsecurityrolepermissionpreauthorize.securityconfig.UserRole.ADMIN;
import static com.example.springsecurityrolepermissionpreauthorize.securityconfig.UserRole.CUSTOMER;
import static com.example.springsecurityrolepermissionpreauthorize.securityconfig.UserRole.MANAGER;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {


    private final PasswordEncoder passwordEncoder;

    /**
     * Burda authorize meselesini method uzerinden edeceyik, bunun ucun birinci novbede
     * @EnableGlobalMethodSecurity(prePostEnabled = true)  true etmeniz ve sonra hansi methodlara kime ne access vereceksizse
     * methodun uzerinde PreAuthorize vererek deyirsiz
     */

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().disable()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*").permitAll() // burda her hansisa bir login teleb etmiyecek
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails elchin = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("el349")
                .password(passwordEncoder.encode("1234"))
                .authorities(ADMIN.getGrantedAuthorities())
                .build();

        UserDetails ilqar = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("ilqar123")
                .password(passwordEncoder.encode("1234"))
                .authorities(MANAGER.getGrantedAuthorities())
                .build();

        UserDetails lale = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("lale123")
                .password(passwordEncoder.encode("1234"))
                .authorities(CUSTOMER.getGrantedAuthorities())
                .build();

        return new InMemoryUserDetailsManager(
                elchin,
                ilqar,
                lale
        );
    }
}
