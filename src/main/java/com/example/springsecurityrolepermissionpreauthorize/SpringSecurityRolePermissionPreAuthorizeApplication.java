package com.example.springsecurityrolepermissionpreauthorize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityRolePermissionPreAuthorizeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityRolePermissionPreAuthorizeApplication.class, args);
    }

}
