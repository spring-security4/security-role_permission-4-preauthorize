package com.example.springsecurityrolepermissionpreauthorize.controller;

import com.example.springsecurityrolepermissionpreauthorize.model.Employee;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("manager/api/v1/employee")
public class EmployeeController {

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    public Employee getById(@PathVariable Long id) {
        return EMPLOYEES.stream()
                .filter(employee -> id.equals(employee.getId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Employee " + id + " not found"));
    }

    @GetMapping
    @PreAuthorize("hasAuthority('employee:read')")
    public List<Employee> getAllEmployee() {
        return EMPLOYEES;
    }

    /**
     *@PreAuthorize("hasRole('ROLE_ADMIN')")  sadece hasRole da verseydik Admin ucun isleyecekdi
     * ancaq ola bielr ki basqa bir role da yaradasizki yalniz create ede bilsin
     * basqalarina toxunmasin bunun ucun hasAuthorityden istifade ede bilerik
     * tebii ki hasAnyRole vermeklede biz bunu ede bilerdik yenede meqsed eyni olacaqdi
     * sadece ola bilerki misal 4 dene role var ve bularin 3 u de bu metodu cagira biler bunun ucun her 3 rolu qeyd etmek evezine
     * sadece authority ni yazmaq kifayet edirki kimin write etmeye icazesi varsa hemin role lar gorecek
     */
    @PostMapping
    @PreAuthorize("hasAuthority('employee:write')")
    public String createEmployee(@RequestBody Employee employee) {
        employee.setEmployeeCode(UUID.randomUUID().toString());
        EMPLOYEES.add(employee);
        return "Employee Created Succesfully";
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('employee:update')")
    public String updateEmployee(@PathVariable Long id,@RequestBody Employee employee){
        return "Updated succesfully";
    }



    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('employee:delete')")
    public String deleteEmployee(@PathVariable Long id){
        return "Employee deleted succesfully";
    }


    private final List<Employee> EMPLOYEES = new ArrayList<>(Arrays.asList(
            new Employee(1L, "Yasin", "Quliyev", UUID.randomUUID().toString()),
            new Employee(2L, "Ismayil", "Camalov", UUID.randomUUID().toString()),
            new Employee(3L, "Fuad", "Ahmadov", UUID.randomUUID().toString())
    ));
}
